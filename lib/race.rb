module RaceBet
  class Race
    SCORES = [15, 10, 5, 3, 1]
    MISPLACED = 1

    class << self
      def score(guesses, winners, scores = SCORES, misplaced = MISPLACED)
        points = 0
        guesses.each_with_index do |guess, i|
          if guess == winners[i]
            points += scores[i]
          elsif winners.include? guess
            points += misplaced
          end
        end
        points
      end
    end
  end
end
